package ru.t1.stroilov.tm.repository;

import ru.t1.stroilov.tm.api.repository.IRepository;
import ru.t1.stroilov.tm.model.AbstractModel;

import java.util.*;
import java.util.function.Predicate;

public abstract class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    protected final Map<String, M> models = new LinkedHashMap<>();

    protected Predicate<M> filterById(final String id) {
        return m -> id.equals(m.getId());
    }

    @Override
    public void deleteAll() {
        models.clear();
    }

    @Override
    public void deleteAll(Collection<M> collection) {
        collection.stream().map(AbstractModel::getId).forEach(models::remove);
    }

    @Override
    public List<M> findAll() {
        return new ArrayList<>(models.values());
    }

    @Override
    public List<M> findAll(final Comparator<M> comparator) {
        final List<M> result = new ArrayList<>(models.values());
        result.sort(comparator);
        return result;
    }

    @Override
    public M add(final M model) {
        models.put(model.getId(), model);
        return model;
    }

    @Override
    public boolean existsById(final String id) {
        return findById(id) != null;
    }

    @Override
    public M findById(final String id) {
        return models.get(id);
    }

    @Override
    public M findByIndex(final Integer index) {
        return models.values().stream().skip(index).findFirst().orElse(null);
    }

    @Override
    public M delete(final M model) {
        if (model == null) return null;
        models.remove(model.getId());
        return model;
    }

    @Override
    public M deleteById(final String id) {
        final M model = findById(id);
        if (model == null) return null;
        return delete(model);
    }

    @Override
    public M deleteByIndex(final Integer index) {
        final M model = findByIndex(index);
        if (model == null) return null;
        return delete(model);
    }

    @Override
    public int getSize() {
        return models.size();
    }
}
