package ru.t1.stroilov.tm.api.service;

import ru.t1.stroilov.tm.api.repository.ITaskRepository;
import ru.t1.stroilov.tm.enumerated.Sort;
import ru.t1.stroilov.tm.enumerated.Status;
import ru.t1.stroilov.tm.model.Task;

import java.util.List;

public interface ITaskService extends ITaskRepository {

    Task updateById(String userId, String id, String name, String description);

    Task updateByIndex(String userId, Integer index, String name, String description);

    Task changeTaskStatusById(String userId, String id, Status status);

    Task changeTaskStatusByIndex(String userId, Integer index, Status status);

    List<Task> findAllByProjectID(String userId, String projectId);

    List<Task> findAll(String userId, Sort sort);

}
